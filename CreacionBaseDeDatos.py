import sqlite3


class Academia:

    def __init__(self):
        self.conexion = sqlite3.connect('academia.db')
        self.cursor = self.conexion.cursor()
        self.cursor.execute("""CREATE TABLE IF NOT EXISTS "articulos" (
        "ID"	INTEGER,
        "Nombre"	TEXT,
        "Carrera"	TEXT,
        "Ciclo"	TEXT,
        "Email"	TEXT,
        PRIMARY KEY("ID" AUTOINCREMENT))
        """)
        self.conexion.commit()
        print("CONEXIÓN ESTABLECIDA")


    def abrir(self):
        conexion = sqlite3.connect("Academia.db")
        return conexion

    def alta(self, datos):
        cone = self.abrir()
        cursor = cone.cursor()
        sql = "insert into articulos(Nombre, ID, Carrera, Ciclo, Email) values (?, ?, ?, ?, ?)"
        cursor.execute(sql, datos)
        cone.commit()
        cone.close()

    def consulta(self, datos):
        try:
            cone = self.abrir()
            cursor = cone.cursor()
            sql = "select Nombre, Carrera, Ciclo, Email from articulos where ID=?"
            cursor.execute(sql, datos)
            return cursor.fetchall()
        finally:
            cone.close()

    def recuperar_todos(self):
        try:
            cone = self.abrir()
            cursor = cone.cursor()
            sql = "select ID, Nombre, Ciclo, Carrera, Email from articulos"
            cursor.execute(sql)
            return cursor.fetchall()
        finally:
            cone.close()

    def baja(self, datos):
        try:
            cone = self.abrir()
            cursor = cone.cursor()
            sql = "delete from articulos where ID=?"
            cursor.execute(sql, datos)
            cone.commit()
            return cursor.rowcount  # retornamos la cantidad de filas borradas
        except:
            cone.close()


if __name__ == "__main__":
    inicio = Academia()